# Veeam Backup & Replication 11.0 补丁许可证资源文件

## 描述

本仓库提供了一系列与 Veeam Backup & Replication 11.0 相关的资源文件，包括补丁许可证、DLL文件等。这些资源文件可以帮助用户在安装和使用 Veeam Backup & Replication 11.0 时进行必要的配置和调整。

## 资源文件列表

1. **VeeamBackup&Replication_11.0.0.837_20210525.iso**  
   Veeam Backup & Replication 11.0 的安装镜像文件。

2. **VeeamBackup&Replication_11_Patch_License**  
   Veeam Backup & Replication 11 的补丁许可证文件。

3. **VeeamOne_11_Patch_License**  
   Veeam One 11 的补丁许可证文件。

4. **VeeamLicense.dll**  
   Veeam 许可证相关的 DLL 文件。

5. **Veeam.One.Common.Managed.dll**  
   Veeam One 通用托管 DLL 文件。

## 安装步骤

1. **安装 Veeam Backup & Replication 11.0**  
   运行 `VeeamBackup&Replication_11.0.0.837_20210525.iso` 文件进行安装。在安装过程中，跳过输入密钥的步骤，选择安装试用版。

2. **替换 DLL 文件**  
   停止所有以 `Veeam` 开头的服务。将 `C:\Program Files\Common Files\Veeam\VeeamLicense.dll` 替换为仓库中的 `VeeamLicense.dll` 文件。

3. **重启服务器**  
   重新启动服务器，并等待所有服务启动。

4. **安装许可证**  
   运行 Veeam Backup & Replication，进入菜单，选择“许可证”，然后点击“安装”，选择 `Veeam_ASv11_1500.lic` 许可证文件进行安装。

## 注意事项

- 请确保在替换 DLL 文件之前停止所有相关的 Veeam 服务，以避免文件被占用。
- 安装过程中请严格按照步骤操作，确保每一步都正确无误。

## 贡献

如果您在使用过程中遇到任何问题或有改进建议，欢迎提交 Issue 或 Pull Request。

## 许可证

本仓库中的资源文件仅供学习和研究使用，请勿用于商业用途。使用本仓库中的资源文件所产生的任何后果，由使用者自行承担。